**Purpose**: A simple circuit and program that lets me remotely cause a 12V motor to turn for a specified amount of time. Why? Because in my case, it allows for pushing a reset button on a stupid false-alarm-generating thing in my basement, which makes a hideous blaring noise in the middle of the night, making me clamber down the stairs, almost fall and kill myself, suffer ear-bleeding noise levels, push the button, all to find that it was a false alarm. (In the extremely unlikely case it's not a false alarm, it can wait until the morning to check on.) Since this is a code-required alarm, I can't modify it, but I can certainly make a robot that pushes the button for me.

See it in action (with no case, to make it clear what it's doing, and also because I don't have the case yet) in the [linked video](https://cdn.discordapp.com/attachments/643609705775890472/679063432724480026/video0.mov).

**Circuit**: I used [these Instructables instructions](https://www.instructables.com/id/12V-Motor-Control-With-5V-Arduino-and-NPN-Transist/) to create the circuit, using exactly the parts listed.

**Parts**: In addition to the resistor and diode in that circuit, and a stock 12V power supply (as well as sockets for it to plug into), I used these parts:

- [Gikfun Solder-able Breadboard](https://smile.amazon.com/gp/product/B071R3BFNL/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)

- [Bridgold 20PCS BD139NPN](https://smile.amazon.com/gp/product/B07Q3S7JWF/ref=ppx_yo_dt_b_asin_title_o07_s00?ie=UTF8&psc=1)

- [DIY Reciprocating Cycle Linear Actuator 12V 15rpm Big Torque](https://smile.amazon.com/gp/product/B07NV8ZVM2/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1) (which provides enough force to push this button)

- [KeeYees ESP8266 NodeMCU CP2102 ESP-12E](https://smile.amazon.com/gp/product/B07HF44GBT/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)

- [AUSTOR 560 Pieces Jumper Wire Kit 14 Lengths Assorted Preformed Breadboard Jumper Wire](https://smile.amazon.com/gp/product/B07CJYSL2T/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) (I found these useful)

**Setting Up**:

- Build the circuit, using D3 for the output pin. Of course you will probably want to breadboard it before you solder it.

- You need to put your own SSID and WEP key in (you'll find them commented out here because I keep mine in a separate `Private.ino` file so you won't see my WEP SSID and key).

- Load the code onto your chip. I find it works best doing it when it's not in the circuit, or at least while pin D3 is not hooked up.

- Set up some kind of port forwarding and MAC reservation, so there's an address you can fire in a web browser that will go to the chip's IP address (ideally one that'll work whether your phone is on your home network or away, because even though you'll mostly use this at home, your phone might be on cell instead of WiFi, or you might have something external call it, say through IFTTT).

- Bookmark that address on your phone, or other device, or save it to the home screen.

- When you load that page, you should get a plain text message, `We are pushing the button now!` and the chip's LED should light up as the motor turns.

- If your motor isn't the same as mine, you might need to change the duration so that it makes one complete cycle and returns to where it started. (See Future Plans below for a better way I hope to implement one day.)

- Temporarily hook the motor directly to 12V so get the plunger fully extended.

- Mount the motor so that it's just barely pushing the button fully in.

- Hook the motor to 12V again until it's fully retracted.

- Hook the motor back to the circuit.

- Optionally, set up an IFTTT applet using Webhooks to call the same URL on some other triggers; For instance, "Alexa, push the button!" Then you don't even have to reach for your phone.

**Future Plans**:

- Add a button that is pushed by the motor as it returns to fully retracted. Instead of running a fixed amount of time, make the code run until that button is pushed. That way the motor never gets out of synch.

- Add a button that activates the circuit, so if I'm standing there I can do it without pulling out my phone, since the motor is a bit in the way of pushing it with my finger.

- Add a noise or light sensor. Make it so the sensor automatically pushes the button, then sends me email, or texts, or fires an IFTTT trigger that turns on a light, etc. (Though a separate sensor that supports IFTTT or web hooks might be better than adding it to this circuit, if a commercially available one is out there.)
