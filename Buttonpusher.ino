// Control a 12V motor linked to a circuit that connects to pin 0

#include <ESP8266WiFi.h>        // Include the Wi-Fi library
#include <WiFiClient.h>
#include <ESP8266WebServer.h>   // Include the WebServer library

// Define the motor's controls
const int MOTOR_PIN = 0;
const int MOTOR_ON = HIGH;
const int MOTOR_OFF = LOW;

// Define the internal LED which serves as an indicator and debug tool
const int LED_PIN =  LED_BUILTIN;
const int LED_ON = LOW;
const int LED_OFF = HIGH;

// Set timing intervals
const int MOTOR_CYCLE_TIME = 3333;
const int QUIET_TIME = 5000;

// WiFi configuration
//const char* ssid     = "YourRouterSSID";
//const char* password = "YourWEPKey";

// Web server setup
ESP8266WebServer server(80);    // Create a webserver object that listens for HTTP request on port 80
void handleRoot();              // function prototypes for HTTP handlers
void handleNotFound();


void setup() {
  // Set the pins to output mode
  pinMode(MOTOR_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

  // connect to WiFi
  Serial.begin(115200);         // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println('\n');
  
  WiFi.begin(ssid, password);             // Connect to the network
  Serial.print("Connecting to ");
  Serial.print(ssid);
  Serial.println(" ...");

  int i = 0;
  while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
    delay(1000);
    Serial.print(++i); Serial.print(' ');
  }

  Serial.println('\n');
  Serial.println("Connection established!");  
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());         // Send the IP address of the ESP8266 to the computer

  // set up a web server
  server.on("/", handleRoot);               // Call the 'handleRoot' function when a client requests URI "/"
  server.onNotFound(handleNotFound);        // When a client requests an unknown URI (i.e. something other than "/"), call function "handleNotFound"

  server.begin();                           // Actually start the server
  Serial.println("HTTP server started"); 
}

void pushTheButton() {
  // do a cycle of running the motor; also show the LED on at the same time
  digitalWrite(LED_PIN, LED_ON);
  digitalWrite(MOTOR_PIN, MOTOR_ON);
  delay(MOTOR_CYCLE_TIME);
  digitalWrite(LED_PIN, LED_OFF);
  digitalWrite(MOTOR_PIN, MOTOR_OFF);
}

void loop() {
    server.handleClient();                    // Listen for HTTP requests from clients
}

void handleRoot() {
  server.send(200, "text/plain", "We are pushing the button now!");   // Send HTTP status 200 (Ok) and send some text to the browser/client
  pushTheButton();
}

void handleNotFound(){
  server.send(404, "text/plain", "404: Not found"); // Send HTTP status 404 (Not Found) when there's no handler for the URI in the request
}
